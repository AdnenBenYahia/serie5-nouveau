package serie5;

import java.io.*;
import java.util.*;

//import com.sun.org.apache.bcel.internal.generic.NEW;

public class Sauvegarde 
{
 //On va sauvegarder les marins dans un fichier
 //cr�ation de la m�thode sauveFichierTexte
 public static void sauveFichierTexte(String nomFichier, Marin marin)
 {
	File fichier = new File(nomFichier);
	Writer writer = null;
	//Ouverture d'un flux de sortie sur un fichier 
	try {
		 writer = new FileWriter(fichier,true); // true ici signifie que le texte sera ajout� a la fin
		 // Cr�ation d'un printWriter sur ce flux
		 PrintWriter pw = new PrintWriter(writer);
         // Ecriture sur fichier
		 pw.append(marin.getNom());
		 pw.append('|');
		 pw.append(marin.getPrenom());
		 pw.append('|');
		 pw.append(String.valueOf(marin.getSalaire()));
		 pw.append('\n'); // pr sauter la ligne
		 pw.close();

		} catch (IOException e) 
	    {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} finally 
	    {
		 if (writer != null) {
		    try {
				  writer.close();
				} catch (IOException e) 
		          {
					// TODO Auto-generated catch block
					e.printStackTrace();
				  }
				}				
			}

		}

		public static List<Marin> lisFichierTexte(String nomFichier){

		 try
			{               
			 //cr�e BufferedReader 
			  BufferedReader br = new BufferedReader( new FileReader(nomFichier));
			  String strLine;
			  StringTokenizer st = null;
			  List <Marin> list = new ArrayList<Marin>();
			  Marin m;

			  //lire ligne par ligne
			  while( (strLine = br.readLine()) != null)
			  {
			   //delimiteur '|'
				st = new StringTokenizer(strLine, "|");
				m = new Marin(st.nextToken(),st.nextToken(),Integer.parseInt(st.nextToken()));
				list.add(m);
			  }
			  br.close();
			  // affichage liste, fonctionne car dan marin, on a surcharg� toString
			  System.out.println("Liste Marins fichier texte: " + list);
			  return list;
			}
			catch(Exception e)
			{
			  System.out.println("Exception en cours de lecture du fichier: " + e);  
			  return null;
			}
		}

		public static void sauveChampBinaire(String nomFichier, Marin marin)
		{
			File fichier = new File(nomFichier);
			OutputStream os = null;

			try {
				os = new FileOutputStream(fichier,true);

				DataOutputStream dos = new DataOutputStream(os);

				dos.writeUTF(marin.getNom());
				dos.writeUTF(marin.getPrenom());
				dos.writeUTF(String.valueOf(marin.getSalaire()));
				dos.close();

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}				
			}
		}

		public static List<Marin> lisChampBinaire(String nomFichier){
			InputStream is = null;
			DataInputStream dis = null;
			List <Marin> list = new ArrayList<Marin>();
			Marin m;

			try{
				// cr�e file input stream
				is = new FileInputStream(nomFichier);
				// cr�e new data input stream
				dis = new DataInputStream(is);

				// while octets available on ecri
				while(dis.available()>0){
					m = new Marin(dis.readUTF(),dis.readUTF(),Integer.parseInt(dis.readUTF()));
					list.add(m);
				}
				// affichage liste, fonctionne car dan marin, on a surcharg� toString
				System.out.println("Liste Marins fichier binaire: " +list);
				return list;

			}

			catch(Exception e){
				e.printStackTrace();
			}finally{
				if(is!=null)
					try {
						is.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if(dis!=null)
					try {
						dis.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return null;
		}

		public static void sauveObjet(String nomFichier, Set<Marin> set){
			File fichier = new File(nomFichier);
			OutputStream os = null;

			try {
				os = new FileOutputStream(fichier,true);
				ObjectOutputStream oos = new ObjectOutputStream(os);
				for(Marin m : set){
					oos.writeObject(m);
				}
				// Si en pram�tre on donne un tableau de marins
				/*for(int i = 0; i<set.size();i++){
					oos.writeObject(marins[i]);
				}*/
				oos.flush();
				oos.close();

			} catch (final java.io.IOException e) {

				e.printStackTrace();

			} finally {
				try {
					if (os != null) {
						os.flush();
						os.close();
					}
				} catch (final IOException ex) {
					ex.printStackTrace();
				}
			}
		}

		public static Set<Marin> lisObjet(String nomFichier){
			InputStream is = null;
			ObjectInputStream ois = null;
			Set <Marin> set = new HashSet<Marin>();
			try{
				// cr�e file input stream
				is = new FileInputStream(nomFichier);
				// cr�e new data input stream
				ois = new ObjectInputStream(is);

				while(is.available() > 0) {
					set.add((Marin) ois.readObject());
				}

				// Apr�s avoir surcharg� toString on affiche la liste
				System.out.println("Liste Marins fichier Objet: " +set);
				return set;

			}

			catch(Exception e){
				e.printStackTrace();

			} finally{
				if(is!=null)
					try {
						is.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if(ois!=null)
					try {
						ois.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return null;
		}


		public static void clearFichier(String nomFichier){
			FileWriter fw = null;
			File fichier = new File(nomFichier);

			try {
				fw = new FileWriter(fichier);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PrintWriter pw = new PrintWriter(fw);
			pw.write("");
			pw.flush(); 
			pw.close();
		}

	}

	
	
