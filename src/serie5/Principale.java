package serie5;
import java.util.HashSet;
import java.util.Set;
public class Principale
{
  public static void main(String[] args)
  {
   String nomFichierTxt = "C:\\Users\\Adnen\\workspace\\serie5\\bin\\serie5\\Files\\marin.txt";
   String nomFichierBin = "C:\\Users\\Adnen\\workspace\\serie5\\bin\\serie5\\Files\\marin.bin";
   String nomFichierObjet = "C:\\Users\\Adnen\\workspace\\serie5\\bin\\serie5\\Files\\marinObjet.data";
   // Ecriture des marins
   Marin m1 = new Marin("sdfi","Rdfg",2450);
   Marin m2 = new Marin("sdfko","sdgfdsa",33000);
   Marin m3 = new Marin("fggr","gfnb",6760);
   Marin m4 = new Marin("Ggfe", "Ddfshan",7860);
   Marin m5 = new Marin("Segien", "Lgbgbt",65700);
  //Marin [] marins = new Marin[5];
  //marins[0] = m1;	marins[1] = m2;	marins[2] = m3;	marins[3] = m4;	marins[4] = m5;
   Set<Marin> set = new HashSet<Marin>();
   // Efface le contenu du fichier ( cr�e juste pour �viter d'effacer chaque fois a la main)
   Sauvegarde.clearFichier(nomFichierTxt);
   Sauvegarde.clearFichier(nomFichierBin);
   Sauvegarde.clearFichier(nomFichierObjet);
  // QUESTION 1 : Fichier Texte
   Sauvegarde.sauveFichierTexte(nomFichierTxt,m1);
   Sauvegarde.sauveFichierTexte(nomFichierTxt,m2);
   Sauvegarde.sauveFichierTexte(nomFichierTxt,m3);
   Sauvegarde.sauveFichierTexte(nomFichierTxt,m4);
   Sauvegarde.sauveFichierTexte(nomFichierTxt,m5);
   Sauvegarde.lisFichierTexte(nomFichierTxt);
  // QUESTION 2 : Fichier Binaire
   Sauvegarde.sauveChampBinaire(nomFichierBin, m1);//sauvegarder ChampBinaire
   Sauvegarde.sauveChampBinaire(nomFichierBin, m2);//sauvegarder ChampBinaire
   Sauvegarde.sauveChampBinaire(nomFichierBin, m3);//sauvegarder ChampBinaire
   Sauvegarde.sauveChampBinaire(nomFichierBin, m4);//sauvegarder ChampBinaire
   Sauvegarde.sauveChampBinaire(nomFichierBin, m5);//sauvegarder ChampBinaire
   Sauvegarde.lisChampBinaire(nomFichierBin); //cr�er listChampBinaire
   //Question 3 : Ecrire & lire les objets
   // On donne en argument une liste marin sinon il y aura un prob pr lire
   set.add(m1);
   set.add(m2);
   set.add(m3);
   set.add(m1);
   //cr�er sauveObjet
   Sauvegarde.sauveObjet(nomFichierObjet, set);
   //cr�er lisObjet
   Sauvegarde.lisObjet(nomFichierObjet);
  }

}

	