package serie5;
import java.io.Serializable;
public class Marin implements Serializable
{
	private static final long serialVersionUID = -7835317879668984235L;
	private String nom ; // nom
	private String prenom ; // prenom
	private int salaire ; // salaire

	public Marin(){
	}
	  public Marin(String n,String p,int s) //le 1er constructeur
	  { 
		 this.nom = n;
		 this.prenom=p;
		 this.salaire=s;
	  }
	  public Marin(String nom,int salaire) //le 2�me constructeur
	  {
	    this(nom," ",salaire); 
	  }
	  
	  @Override
	public String toString() 
	{
		return "Marin [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + salaire;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (salaire != other.salaire)
			return false;
		return true;
	}
	public String getNom() 
	  { 
	    return nom ;
	  }
	  public String getPrenom() 
	  { 
	    return prenom ;
	  }
	  public int getSalaire() 
	  { 
	    return salaire;
	  }
	  public void setNom(String nom) 
	  {
	    this.nom = nom ;
	  }		
	  public void setPrenom(String prenom) 
	  {
	    this.prenom = prenom ;
	  }		
	  public void setSalaire(int salaire) 
	  {
	    this.salaire = salaire ;
	  }		
	  
	  public int augmenteSalaire(int augmentation)
	  {
		 salaire=salaire+augmentation;
		 return salaire; 
	  }
	  public Object clone() throws CloneNotSupportedException{
		  return super.clone();
	  }

}
